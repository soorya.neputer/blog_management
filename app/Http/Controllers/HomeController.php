<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       // return view('home');
      return redirect()->route(request()->user()->role);
    }


    public function admin()
    {
      return view('dashboard.index');
      // make admin folder and make dashborad.blade.php
    }

    public function seller()
    {
      return view('dashboard.seller');
      // make admin folder and make dashborad.blade.php
    }

      public function customer()
    {
      return view('dashboard.customer');
      // make admin folder and make dashborad.blade.php
    }
}
