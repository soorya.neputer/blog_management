<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/admin', [App\Http\Controllers\HomeController::class, 'admin'])->name('admin');
});


Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'seller']], function () {
    Route::get('/seller', [App\Http\Controllers\HomeController::class, 'seller'])->name('seller');
});

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'customer']], function () {
    Route::get('/customer', [App\Http\Controllers\HomeController::class, 'customer'])->name('customer');
});
