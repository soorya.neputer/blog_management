<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = array(
            array(
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('112112'),
                'role' => 'admin',
                'status' => 'active',
            ),


            array(
                'name' => 'seller',
                'email' => 'seller@gmail.com',
                'password' => bcrypt('221221'),
                'role' => 'seller',
                'status' => 'active',
            ),

            array(
                'name' => 'customer',
                'email' => 'customer@gmail.com',
                'password' => bcrypt('332332'),
                'role' => 'customer',
                'status' => 'active',
            ));

        foreach ($user as $user_list) {
            if (User::where('email', $user_list['email'])->count() <= 0) {
                User::insert($user_list);
            }
        }
    }
}
